#!/bin/bash
#Various things I want installed when possible on my machines
# TODO : differentiate OSX from other *NIX systems (dot file for bash), or find a way to always use the same
APPs=vim git htop screen openssh p7zip-full

sudo apt-get install aptitude
sudo aptitude install $APPS

mkdir -p ~/conf && cd ./conf
git clone https://MartinBahier@bitbucket.org/MartinBahier/tuxconf.git
cp .[a-zA-Z]+ ~/
source ~/.bash_login
screen
